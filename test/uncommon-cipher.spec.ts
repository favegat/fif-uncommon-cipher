import * as chai from 'chai';
import * as cryptoJS from 'crypto-js';
import {UnCommonCipher} from '../src/uncommon-cipher/uncommon-cipher';

describe('common-cipher', (): void => {
    describe('methods encryptStringWithRSAPublicKey and decryptStringWithRSAPrivateKey', (): void => {

        describe('method decryptAllValuesFromObject', (): void => {
            it('Should return an object with all their atributes decrypted', (): void => {
                const bodyIn = {
                    'parameter1': 'U2FsdGVkX1/TRPnrmG80mJ40K8nccGrmazOm8j4jIpI='
                };
                const bodyOut = {
                    'parameter1': '1'
                };
                const sessionKey = 'sessionKey';

                const descifrado = UnCommonCipher.decryptAllValuesFromObject(bodyIn, sessionKey);
                chai.assert.equal(descifrado.parameter1, bodyOut.parameter1);
            });
        });

        describe('method encryptAllValuesFromObject', (): void => {
            it('Should return an object with all their atributes encrypted', (): void => {
                const bodyIn = {
                    'nombre': 'orlando',
                    'apellido': 'bustos',
                    'cumpleaños': '06/06'
                };
                const sessionKey = 'sessionKey';

                const cifrado = UnCommonCipher.encryptAllValuesFromObject(bodyIn, sessionKey);

                const descifrado1 = cryptoJS.AES.decrypt(cifrado.nombre, sessionKey).toString(cryptoJS.enc.Utf8);
                const descifrado2 = cryptoJS.AES.decrypt(cifrado.apellido, sessionKey).toString(cryptoJS.enc.Utf8);
                const descifrado3 = cryptoJS.AES.decrypt(cifrado.cumpleaños, sessionKey).toString(cryptoJS.enc.Utf8);

                chai.expect(descifrado1).to.equals('orlando');
                chai.expect(descifrado2).to.equals('bustos');
                chai.expect(descifrado3).to.equals('06/06');
            });
        });

        describe('method sha256', (): void => {
            it('Should return ', (): void => {
                const textoADigerir = 'Texto a digerir';
                const expectedResult = 'a83eb0ec95daa8b83f72bd21ebb5087ec90c70819f573ba9bcd16670b5d8876c';

                const textoDigerido = UnCommonCipher.sha256(textoADigerir);

                chai.expect(textoDigerido).to.be.equals(expectedResult);
            });
        });

        describe('method generateRandomKey', (): void => {
            it('Should return ', (): void => {
                const keyLength = 256;
                const llaveDeEjemplo = '4ff3a6cd2cb0ab5440e2d0563d0b07f234905781441785ea70609b3b4f8121f7';

                const llaveGenerada = UnCommonCipher.generateRandomKey(keyLength);

                chai.expect(llaveGenerada).to.be.a('string');
                chai.expect(llaveGenerada.length).to.be.equal(llaveDeEjemplo.length);
            });
        });
    });
});
