import {AES, enc, lib, SHA256} from 'crypto-js';
import * as CryptoJS from 'crypto-js';

export namespace UnCommonCipher {
  let publicKey: any;
  
  const getPublicKey = () => {
    if (publicKey) {
      return publicKey;
    }
    return false;
  };

  export function setPublicKey(keyToSet:string) {
    if (keyToSet) {
      publicKey = {
        key: keyToSet,
          // Using constants.RSA_PKCS1_PADDING = 1
        padding: 1
      };
      return true;
    }
    return false;
  }

  export function encryptAllValuesFromObject(body:any, key:string) {
    const bodyEnc = { ...body };
    const parametersNames = Object.keys(body);
    parametersNames.forEach((parameter:any) => {
      bodyEnc[parameter] = AES.encrypt(typeof (body[parameter]) !== 'string' ? JSON.stringify(body[parameter]) : body[parameter], key).toString();
    });
    return bodyEnc;
  }

  export function decryptAllValuesFromObject(bodyIn:any, sessionKey:string) {
    const bodyOut = { ...bodyIn };
    const atributes = Object.keys(bodyIn);
    atributes.forEach((atribute:any) => {
      const field = bodyIn[atribute];
      bodyOut[atribute] = AES.decrypt(field, sessionKey).toString(enc.Utf8);
    });
    return bodyOut;
  }

  export function sha256(textToDigest: string): string {
    return SHA256(textToDigest).toString();
  }

  export function generateRandomKey(keyLength: number): string {
      return ((lib.WordArray as any).random(keyLength / 8) as CryptoJS.WordArray).toString();
  }

}
